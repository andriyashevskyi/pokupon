import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
  state = {
    inputValue: "",
    selectValue: "-",
    error:false,
    errMsg:''
  };

  getSearchResults = (engine, request) => {
    let searchEngines = {
      g: "google.com/search?q",
      b: "bing.com/search?q",
      a: "uk.ask.com/web?q"
    };
    let url = `https://${searchEngines[engine]}=${request}`;
    let newTab = window.open(url, "_blank");
    newTab.focus();
  };

  inputHandler = e => {
    this.setState({
      inputValue: e.target.value
    });
  };

  selectHandler = e => {
    this.setState({
      selectValue: e.target.value
    });
  };

  render() {
    const { inputValue, selectValue, error,errMsg } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="inputs-block">
          <input placeholder="Что ищем?" type="text" value={inputValue} onChange={this.inputHandler} />

          <select value={selectValue} onChange={this.selectHandler}>
            <option value="-">-</option>
            <option value="g">Google.com</option>
            <option value="b">Bing.com</option>
            <option value="a">Ask.com</option>
          </select>

          <button
            onClick={e => {
              e.preventDefault();
              if (!!inputValue === false || selectValue === "-") {
                this.setState({
                  error: true,
                  errMsg: "Пожалуйста, выберите поисковую систему и введите запрос"
                });
              }
              else{
                this.setState({
                  error:false
                })
                this.getSearchResults(selectValue,inputValue);

              }
            }}
          >
            get results
          </button>

          {error?<p style={{color:'red'}}>{errMsg}</p>:null}
        </div>
      </div>
    );
  }
}

export default App;
